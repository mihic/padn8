package padn8;

import java.util.LinkedList;

public class Padn8 {

    public static void main(String[] args) {
        vozlisce koren = new vozlisce(20);

        koren.dodaj(25);
        koren.dodaj(15);
        koren.dodaj(14);
        koren.dodaj(13);
        koren.dodaj(12);
        koren.dodaj(16);
        koren.dodaj(17);
        koren.dodaj(18);
        koren.dodaj(19);
        koren.izpis();
        System.out.println();
        System.out.println(koren.visina());
        System.out.println(koren.sirina()-1);
        koren.izpisiNaRazdalji(3);

    }
}

class vozlisce {

    public vozlisce left;
    public vozlisce right;
    public int vrednost;

    vozlisce(int vrd, vozlisce l, vozlisce d) {
        this.vrednost = vrd;
        this.left = l;
        this.right = d;
    }

    vozlisce(int vrd) {
        this.vrednost = vrd;
        this.left = null;
        this.right = null;
    }

    void izpisiNaRazdalji(int n) {
        LinkedList levi = new LinkedList();
        LinkedList desni = new LinkedList();

        if (this.right != null) {
            levi.add(this.vrednost);
            this.right.elementiNaRazdalji(n - 1, desni);
            izpisikombinacij(levi, desni);
            levi.clear();
            desni.clear();
        }

        if (this.left != null) {
            desni.add(this.vrednost);
            this.left.elementiNaRazdalji(n - 1, levi);
            izpisikombinacij(levi, desni);
            levi.clear();
            desni.clear();
        }

        if (this.right != null && this.left != null) {
            for (int i = 1; i <= n - 1; i++) {
                this.left.elementiNaRazdalji(i - 1, levi);
                this.right.elementiNaRazdalji(n - i - 1, desni);
                izpisikombinacij(levi, desni);
                levi.clear();
                desni.clear();
            }
        }

        if (this.left != null) {
            this.left.izpisiNaRazdalji(n);
        }
        if (this.right != null) {
            this.right.izpisiNaRazdalji(n);
        }

    }

    static void izpisikombinacij(LinkedList levi, LinkedList desni) {
        for (int i = levi.size() - 1; i >= 0; i--) {
            for (int j = desni.size() - 1; j >= 0; j--) {
                System.out.println(levi.get(i) + " " + desni.get(j));
            }
        }
    }

    void elementiNaRazdalji(int n, LinkedList kam) {
        if (n == 0) {
            kam.add(this.vrednost);
        }
        if (this.left != null) {
            this.left.elementiNaRazdalji(n - 1, kam);
        }
        if (this.right != null) {
            this.right.elementiNaRazdalji(n - 1, kam);
        }
    }

    int sirina() {
        int levi = 0;
        int desni = 0;
        int skozi = 0;
        if (this.left == null && this.right == null) {
            return 1;
        }
        if (this.left != null) {
            levi = this.left.sirina();
            skozi += this.left.visina();
        }
        if (this.right != null) {
            desni = this.right.sirina();
            skozi += this.right.visina();
        }
        skozi++;
        if (levi > desni && levi > skozi) {
            return levi;
        } else if (desni > levi && desni > skozi) {
            return desni;
        } else {
            return skozi;
        }
    }

    int visina() {
        int levi = 0;
        int desni = 0;
        if (this.left == null && this.right == null) {
            return 1;
        }
        if (this.left != null) {
            levi = 1 + this.left.visina();
        }
        if (this.right != null) {
            desni = 1 + this.right.visina();
        }
        return Math.max(levi, desni);

    }

    void izpis() {
        System.out.print(this.vrednost + "(");
        if (this.left == null) {
            System.out.print("null ");
        } else {
            this.left.izpis();
        }
        if (this.right == null) {
            System.out.print("null");
        } else {
            this.right.izpis();
        }
        System.out.print(")");
    }

    void dodaj(int vrd) {
        if (vrd == this.vrednost) {
        } else if (vrd < this.vrednost) {
            if (this.left == null) {
                this.left = new vozlisce(vrd);
            } else {
                this.left.dodaj(vrd);
            }
        } else {
            if (this.right == null) {
                this.right = new vozlisce(vrd);
            } else {
                this.right.dodaj(vrd);
            }
        }
    }
}
